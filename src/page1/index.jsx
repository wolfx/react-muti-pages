import React from "react";
import ReactDOM from "react-dom";
import "./style.less";

import logo from "./logo.jpg";

ReactDOM.render(
  <div className="demo">
    <span className="green">page122</span>
    <img src={logo} alt=""/>
  </div>,
  document.getElementById("root")
);
